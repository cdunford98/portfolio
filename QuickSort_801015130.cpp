//
//  QuickSort_Skeleton.cpp
//
//  Created by Bahamon, Julio on 6/25/19.
//  UNC Charlotte
//  Copyright © 2019 Bahamon, Julio. All rights reserved.
//

#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

//  Declaring a new struct to store patient data
struct patient {
    int age;
    char name[20];
    float balance;
};


//  TODO:
//  IMPLEMENT A FUNCTION THAT COMPARES TWO PATIENTS BY AGE

//  THE FUNCTION RETURNS AN INTEGER AS FOLLOWS:
//      -1 IF THE AGE OF THE FIRST PATIENT IS LESS
//         THAN THE SECOND PATIENT'S AGE
//       0 IF THE AGES ARE EQUAL
//       1 OTHERWISE
int compareAge(const void * p1, const void * p2)
{

 
  if ((((patient*)p1)->age) <  ((((patient*)p2)->age))) {
  return -1;
  }
  if ((((patient*)p1)->age) ==  (((patient*)p2)->age)) {
  return 0;
  }
  else {
    return 1;
  }
}


//  TODO:
//  IMPLEMENT A FUNCTION THAT COMPARES TWO PATIENTS BY BALANCE DUE

//  THE FUNCTION RETURNS AN INTEGER AS FOLLOWS:
//      -1 IF THE BALANCE FOR THE FIRST PATIENT IS LESS
//         THAN THE SECOND PATIENT'S BALANCE
//       0 IF THE BALANCES ARE EQUAL
//       1 OTHERWISE
int compareBalance(const void * p1, const void * p2)
{

 
  if ((((patient*)p1)->balance) <  ((((patient*)p2)->balance))) {
  return -1;
  }
  if ((((patient*)p1)->balance) ==  (((patient*)p2)->balance)) {
  return 0;
  }
  else {
    return 1;
  }
}

//  TODO:
//  IMPLEMENT A FUNCTION THAT COMPARES TWO PATIENTS BY NAME

//  THE FUNCTION RETURNS AN INTEGER AS FOLLOWS:
//      -1 IF THE NAME OF THE FIRST PATIENT GOES BEFORE
//         THE SECOND PATIENT'S NAME
//       0 IF THE AGES ARE EQUAL
//       1 OTHERWISE
int compareName(const void * p1, const void * p2)
{
  patient* obj1 = (patient*)p1;
  patient* obj2 = (patient*)p2;
  int n;
 for (n=0 ; n<3 ; n++)
    if (strncmp (obj1->name, obj2->name,20) < 0)
    {
      return -1;
    }
    if (strncmp (obj1->name, obj2->name,20) == 0)
    {
      return 0;
    }
    else {
      return 1;
    }
}

  
 
//  HINT: USE THE strncmp FUNCTION
//  (SEE http://www.cplusplus.com/reference/cstring/strncmp/)


//  The main program
int main()
{
 
    int total_patients = 5;
    
    //  Storing some test data
    struct patient patient_list[5] = {
        {25, "Juan Valdez   ", 1250},
        {15, "James Morris  ", 2100},
        {32, "Tyra Banks    ", 750},
        {62, "Maria O'Donell", 375},
        {53, "Pablo Picasso ", 615}
    };
  
    
    cout << "Patient List: " << endl;
    
    //  TODO:
    //  IMPLEMENT THE CODE TO DISPLAY THE CONTENTS
    //  OF THE ARRAY BEFORE SORTING
    for(int i = 0; i <= 4; i++){
      cout << patient_list[i].age << "  " << patient_list[i].name << patient_list[i].balance <<" " << " "<< endl;
    }
    cout << endl;
    
    
    cout << "Sorting..." << endl;
    
    //  TODO:
    //  CALL THE qsort FUNCTION TO SORT THE ARRAY BY PATIENT AGE

 

    cout << "Patient List - Sorted by Age: " << endl;
      int n;
  qsort (patient_list, 5, sizeof(patient), compareAge);
  for (n=0; n<5; n++){
          cout << patient_list[n].age << "  " << patient_list[n].name << patient_list[n].balance <<" " << " "<< endl;
  } 
    //  TODO:
    //  DISPLAY THE CONTENTS OF THE ARRAY
    //  AFTER SORTING BY AGE
    
    cout << endl;
    
    
    cout << "Sorting..." << endl;
    
    //  TODO:
    //  CALL THE qsort FUNCTION TO SORT THE ARRAY BY PATIENT BALANCE
    
    cout << "Patient List - Sorted by Balance Due: " << endl;
      
  qsort (patient_list, 5, sizeof(patient), compareBalance);
  for (n=0; n<5; n++){
           cout << patient_list[n].age << "  " << patient_list[n].name << patient_list[n].balance <<" " << " "<< endl;
  }
    //  TODO:
    //  DISPLAY THE CONTENTS OF THE ARRAY
    //  AFTER SORTING BY BALANCE
    
    cout << endl;
    
    
    cout << "Sorting..." << endl;
    
    //  TODO:
    //  CALL THE qsort FUNCTION TO SORT THE ARRAY BY PATIENT NAME
    
    cout << "Patient List - Sorted by Name: " << endl;
    
    //  TODO:
    //  DISPLAY THE CONTENTS OF THE ARRAY
    //  AFTER SORTING BY NAME
    //int n;
  qsort (patient_list, 5, sizeof(patient), compareName);
  for (n=0; n<5; n++){
        cout << patient_list[n].age << "  " << patient_list[n].name << patient_list[n].balance <<" " << " "<< endl;
  }

    
    return 0;
}