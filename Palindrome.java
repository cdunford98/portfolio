//CSC 151 - Chase Dunford - Chap7 Ex10 (Palindrome)
import java.util.Scanner;

public class Palindrome
{
   public static void main(String[] args)
   {  
      //Declaring the variables.
      String userInput;
      String palindrome = "";
      int spaces = 0;
    
      Scanner input = new Scanner(System.in);
      
      System.out.print("Please enter a word or phrase: ");
      userInput = input.nextLine(); 
      
      int length = userInput.length();
      
         
      for(int x = length - 1; x >= 0; x--) 
      palindrome = palindrome + userInput.charAt(x);
      {
         for(int i = 0; i < userInput.length(); i++)
         {
            if(userInput.charAt(i) == ' ')
            ++spaces;
         }
      }
 
   
      if(userInput.equalsIgnoreCase(palindrome))
      {
         System.out.println(userInput + " is a palindrome.");
      }
  
      if(!userInput.equalsIgnoreCase(palindrome))
      {
         System.out.println(userInput + " is not a palindrome.");       
      } 
     
    }
}

