import java.util.Scanner;

public class InchesToFeetInteractive
{
   public static void main(String[] args)
   {
      int inches;
      int feet;
      int remainder;
      
      Scanner input = new Scanner(System.in);
      
      //Asks the user to enter a number of inches.
      System.out.print("How many inches do you have? ");
      inches = input.nextInt();
      
      //Divides the number of inches by 12.
      feet = inches/12;
      
      //Produces the leftover inches that do not make a foot.
      remainder = inches%12;
      
      System.out.println(inches + " inches " + " becomes " +
      feet + " feet" + " and " + remainder + " inches.");
                  
   }
}
